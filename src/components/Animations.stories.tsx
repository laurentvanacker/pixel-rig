import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import Animations, {AnimationsProps} from './Animations';

export default {
  title: 'modals/Animations',
  component: Animations,
} as Meta;

const Template: Story<AnimationsProps> = (args: AnimationsProps) => <Animations {...args} />;

export const Default = Template.bind({});
Default.args = {
  animations: []
};

