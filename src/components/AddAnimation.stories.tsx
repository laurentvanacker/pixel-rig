import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import AddTemplateAnimationModal, {AddAnimationProps} from 'components/AddTemplateAnimationModal';

export default {
  title: 'modals/AddAnimationModal',
  component: AddTemplateAnimationModal,
} as Meta;

const Template: Story<AddAnimationProps> = (args) => <AddTemplateAnimationModal {...args} />;

export const Default = Template.bind({});
Default.args = {
  visible: false,
};

