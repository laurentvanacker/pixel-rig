import React, {useEffect, useRef, useState} from 'react';
import tw from 'twin.macro';
import {IAnimation, deleteAnimation} from "store/slices/animations.slice";
import Animation from './Animation';
import {useDispatch, useSelector} from "react-redux";
import EditAnimationModal from "components/EditAnimation.modal";
import {RootState} from "store/store";
const {ipcRenderer} = window.require("electron");


export interface AnimationsProps {
  animations: IAnimation[]
}


const AnimationsContainer = tw.div`bg-gray-200 mx-auto h-full h-60 text-gray-400`;

function Animations({ animations }: AnimationsProps) {
  const [visible, setVisible] = useState(false);
  const [editingAnimation, setEditingAnimation] = useState<IAnimation>()

  const backgroundSprite = useSelector(((s: RootState) => s.animations.backgroundImage))
  const [backgroundImage, setBackgroundImage] = useState<HTMLImageElement>()

  useEffect(() => {
    if (backgroundSprite) {
      const image = new Image();
      image.onload = ((ev: Event) => {
        console.log(`Background image loaded`)
      });
      image.src = `data:image/png;base64, ${backgroundSprite.contents}`
      setBackgroundImage(image);
    }
  }, [backgroundSprite])

  const dispatch = useDispatch()

  if (animations.length === 0) {
    // TODO I'm sure this can be cleaner...
    return (<AnimationsContainer>
      <div tw="flex justify-between flex-wrap w-full text-center h-60 content-center">
        <div></div>
        <div>
          No animations yet
        </div>
        <div></div>
      </div>
    </AnimationsContainer>)
  }

  const onDeleteAnimation = (animationName: string) => {
    ipcRenderer.invoke("remove-animation", animationName)
    dispatch(deleteAnimation(animationName))
  }

  const onEditAnimation = (animation: IAnimation) => {
    setEditingAnimation(animation)
    setVisible(true)
  }

  return (
    <AnimationsContainer>
      <EditAnimationModal visible={visible} setVisible={setVisible} animation={editingAnimation} />
      <div tw="flex flex-row p-1">
        {animations.map((animation) => <Animation
            key={animation.name}
            animation={animation}
            onDeleteAnimation={onDeleteAnimation}
            onEditAnimation={onEditAnimation}
            backgroundImage={backgroundImage}
          />
        )}
      </div>
    </AnimationsContainer>
  );
}

export default Animations;