import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import AnimationBar, {AnimationBarProps} from './AnimationBar';

export default {
  title: 'modals/AnimationBar',
  component: AnimationBar,
} as Meta;

const Template: Story<AnimationBarProps> = (args: AnimationBarProps) => <AnimationBar {...args} />;

export const Default = Template.bind({});
Default.args = {
};

