import React, {ElementRef, ReactNode, useEffect, useRef, useState} from 'react';
import tw from 'twin.macro';
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "store/store";
import {
  updateAnimationProperties,
  IAnimation,
  IAnimationProperties,
  ISprite,
} from "store/slices/animations.slice";
import {Button} from "components/Buttons";

import {ZoomIn} from "@styled-icons/bootstrap/ZoomIn"
import {ZoomOut} from "@styled-icons/bootstrap/ZoomOut"
import {FastForward} from "@styled-icons/material/FastForward"
import {FastRewind} from "@styled-icons/material/FastRewind"
import {AnimationSent, ImageSent} from "App";
import Timer from "components/Timer";

const {ipcRenderer} = window.require("electron");

// import tw from 'twin.macro';

export interface EditAnimationProps {
  visible: boolean;
  setVisible: (b: boolean) => void;
  animation?: IAnimation;
}

// const Div = tw.div`border mx-auto bg-purple-300 hover:border-black hover:bg-purple-500 rounded-lg py-2 px-4 text-white max-w-lg`


const CanvasButton = tw.button`text-sm px-1 m-0 rounded border border-black bg-gray-200 hover:bg-gray-300 text-black`


function EditAnimationModal({visible, setVisible, animation}: EditAnimationProps) {
  const [spriteIndex, setSpriteIndex] = useState(0)
  const [name, setName] = useState("")
  const [button, setButton] = useState("")
  const [properties, setProperties] = useState<IAnimationProperties>({
    flip: false,
    scale: 1,
    timeout: 100,
  })

  const [images, setImages] = useState<HTMLImageElement[]>([]);

  const dispatch = useDispatch()

  const canvasRef = useRef<HTMLCanvasElement | null>(null)

  useEffect(() => {
    if (animation) {
      setName(animation.name);
      setButton(animation.button);
      setProperties(animation.properties);
      setImages(animation.sprites.map((sprite: ISprite) => {
        const image = new Image()
        image.onload = (ev) => {
          console.log(`Image loaded ${sprite.filePath}`)
        }
        image.src = `data:image/png;base64, ${sprite.contents}`
        return image;
      }));
    }
  }, [animation])

  const draw = (context: CanvasRenderingContext2D) => {
    if (images.length === 0) {
      return;
    }

    const image = images[spriteIndex];
    const {flip, scale} = properties;
    context.fillStyle = "#000000"
    context.fillRect(0, 0, context.canvas.width, context.canvas.height)
    if (images.length > 0) {
      if (flip) {
        context.setTransform(-1, 0, 0, 1, (context.canvas.width), 0)
        context.drawImage(image, (context.canvas.width - image.width * scale) / 2, (context.canvas.height - image.height * scale) / 2, image.width * scale, image.height * scale)
      } else {
        context.setTransform(1, 0, 0, 1, 0, 0)
        context.drawImage(image, (context.canvas.width - image.width * scale) / 2, (context.canvas.height - image.height * scale) / 2, image.width * scale, image.height * scale)
      }
    }
  }

  useEffect(() => {
    if (!visible) {
      return
    }
    const canvas = canvasRef.current as HTMLCanvasElement
    const context = canvas.getContext('2d')
    //

    if (context !== null) {
      context.imageSmoothingEnabled = false
      // draw(context)
      let animationFrameId: number;

      const render = () => {
        draw(context)
        animationFrameId = window.requestAnimationFrame(render)
      }
      render()

      return () => {
        window.cancelAnimationFrame(animationFrameId)
      }
    }
  }, [draw])

  const hide = () => {
    setVisible(false);
  }

  const save = () => {
    dispatch(updateAnimationProperties({
      sprites: animation!.sprites,
      name,
      button,
      properties,
    }))
    setVisible(false)
  }

  const onTimeout = () => {
    if (visible && images.length > 0) {
      setSpriteIndex((spriteIndex + 1) % images.length)
    }
  }

  const onIncreaseScale = () => {
    setProperties({
      ...properties,
      scale: properties.scale * 2,
    })
  }

  const onDecreaseScale = () => {
    setProperties({
      ...properties,
      scale: properties.scale / 2,
    })
  }

  const resetScale = () => {
    setProperties({
      ...properties,
      scale: 1,
    })
  }

  const onSlowDown = () => {
    const updateRate = 1000 / properties.timeout;
    setProperties({
      ...properties,
      timeout: Math.round(1000 / (Math.max(updateRate - 1, 1))),
    })
  }

  const onSpeedUp = () => {
    const updateRate = 1000 / properties.timeout;
    setProperties({
      ...properties,
      timeout: Math.round(1000 / (updateRate + 1)),
    })
  }

  const resetSpeed = () => {
    setProperties({
      ...properties,
      timeout: 200,
    })
  }

  const toggleFlip = () => {
    setProperties({
      ...properties,
      flip: !properties.flip,
    })
  }

  if (!visible) {
    return null;
  }

  return (
    <div tw="fixed z-10 inset-0 overflow-y-auto font-sans">
      <div tw="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div tw="fixed inset-0 transition-opacity" aria-hidden="true">
          <div tw="absolute inset-0 bg-gray-800 opacity-60"></div>
        </div>

        <span tw="sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

        <div
          tw="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all my-8 align-middle max-w-md w-full"
          role="dialog" aria-modal="true" aria-labelledby="modal-headline">
          <div tw="bg-white px-4 pt-5 pb-4 p-6 pb-4">
            <div tw="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
              <h3 tw="text-lg leading-6 font-medium text-gray-900 text-center" id="modal-headline">
                Edit animation {`${name.length > 0 ? name[0].toUpperCase() : ""}${name.slice(1)} - ${button}`}
              </h3>
              <div tw="flex flex-row justify-center content-center max-w-lg w-full mt-2">
                {/*<div tw="flex flex-col justify-center">*/}
                {/*  <div tw="flex flex-row">*/}
                {/*    <p tw="flex">{name}</p>*/}
                {/*    <p tw="flex">{button}</p>*/}
                {/*  </div>*/}
                {/*</div>*/}
                <div tw="flex content-center text-gray-900 content-center">
                  <div>
                    <div tw="">
                      <div tw="flex items-center justify-center align-top">
                        <CanvasButton tw="mt-0" onClick={toggleFlip}>{properties.flip ? "Flipped" : "Not Flipped"}</CanvasButton>
                      </div>
                      <div tw="flex items-center justify-center align-top">
                        <CanvasButton onClick={onSlowDown}><FastRewind tw="w-4 h-4" fill="black"/></CanvasButton>
                        <CanvasButton tw="mt-0" onClick={resetSpeed}>{Math.round(1000 / properties.timeout).toFixed(0)} Hz</CanvasButton>
                        <CanvasButton onClick={onSpeedUp}><FastForward tw="w-4 h-4" fill="black"/></CanvasButton>
                      </div>
                      <canvas tw="border border-black" width="144px" height="144px" ref={canvasRef}/>
                      <div tw="flex items-center justify-center align-top">
                        <CanvasButton onClick={onDecreaseScale}><ZoomOut tw="w-4 h-4" fill="black"/></CanvasButton>
                        <CanvasButton tw="mt-0" onClick={resetScale}>{(properties.scale * 100).toFixed(1)} %</CanvasButton>
                        <CanvasButton onClick={onIncreaseScale}><ZoomIn tw="w-4 h-4" fill="black"/></CanvasButton>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div tw="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
            <button onClick={save} type="button"
                    tw="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-400 text-base font-medium text-white hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm">
              Save
            </button>
            <button onClick={hide} type="button"
                    tw="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
              Cancel
            </button>
          </div>
          <Timer timeout={properties.timeout} onTimeout={onTimeout}/>
        </div>
      </div>
    </div>
  );
}

export default EditAnimationModal;