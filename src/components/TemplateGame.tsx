import Phaser, {Game, Scene} from 'phaser';
import React, {useEffect, useRef, useState} from 'react';
import {useSelector} from "react-redux";
import {RootState} from "store/store";
import {IAnimation} from "store/slices/animations.slice";
import {ISprite} from "../store/slices/animations.slice";




export interface GameProps {
  width: number;
  height: number;
}


export interface TemplateInterface extends Phaser.Scene {
  animations: IAnimation[];
  shouldUpdateTextures: boolean;
  animationKeyMap: Record<string, string>
}

class Template extends Phaser.Scene implements TemplateInterface {
  animations: IAnimation[] = [];
  backgroundImage?: ISprite = undefined;

  shouldUpdateTextures = false;
  shouldUpdateAnimations = false;
  shouldLoadBackgroundImage = false;
  shouldUpdateBackgroundImage = false;

  currentAnimation: string | undefined = ""

  animationKeyMap = Object();
  player: Phaser.GameObjects.Sprite | undefined = undefined;
  background: Phaser.GameObjects.Sprite | undefined = undefined;

  cursors: Phaser.Types.Input.Keyboard.CursorKeys | undefined = undefined

  loadedTextures: Record<string, boolean> = {}

  preload = () => {
    console.log(this);
    console.log("Preloaded in Phaser")
    this.load.image('default', 'assets/boat_LR_1.png')
  }


  create = () => {
    this.cursors = this.input.keyboard.createCursorKeys()
    this.background = this.add.sprite(this.cameras.main.centerX, this.cameras.main.centerY, 'background')
    this.background.setScale(4, 4)
    this.player = this.add.sprite(this.cameras.main.centerX, this.cameras.main.centerY, 'default')
  }

  update(time: number, delta: number) {
    super.update(time, delta);

    const keys = this.animations.map((animation) => animation.button)

    let animation: IAnimation | undefined;

    ["up", "down", "right", "left"].forEach((key) => {
      // @ts-ignore
      if (this.cursors && this.cursors[key].isDown) {
        if (keys.includes(key)) {
          animation = this.animations.find((animation) => animation.button === key)!;
          this.currentAnimation = key
        }
      }
    })

    // TODO
    // if (this.cursors?.up.isDown) {
    //   if (keys.includes('up')) {
    //     animation = this.animations.find((animation) => animation.button === 'up')!;
    //   }
    // }
    // if (this.cursors?.down.isDown) {
    //   if (keys.includes('down')) {
    //     animation = this.animations.find((animation) => animation.button === 'down')!;
    //   }
    // }
    // if (this.cursors?.left.isDown) {
    //   if (keys.includes('left')) {
    //     animation = this.animations.find((animation) => animation.button === 'left')!;
    //   }
    // }
    // if (this.cursors?.right.isDown) {
    //   if (keys.includes('right')) {
    //     animation = this.animations.find((animation) => animation.button === 'right')!;
    //   }
    // }

    if (animation) {
      if (animation.properties?.flip) {
        this.player?.setScale(-4, 4)
      } else {
        this.player?.setScale(4, 4)
      }

      if (!this.player?.anims.isPlaying || this.player?.anims.currentAnim.key !== this.animationKeyMap[animation!.name]) {
        this.player?.play(this.animationKeyMap[animation.name]);
      }
    }



    if (this.shouldUpdateTextures) {
      this.updateTextures()
      this.shouldUpdateTextures = false
    }

    if (this.shouldUpdateAnimations) {
      this.updateAnimations()
      this.shouldUpdateAnimations = false;
      if (this.currentAnimation) {
        this.player?.play(this.animationKeyMap[this.currentAnimation]);
      }
    }

    if (this.shouldLoadBackgroundImage) {
      this.updateBackgroundImage()
      this.shouldLoadBackgroundImage = false;
    }

    if (this.shouldUpdateBackgroundImage) {
      this.background?.setTexture(this.animationKeyMap[`background`])
    }
  }

  onBackgroundTextureLoaded(key: string) {
    console.log(`Updated background image`)
    this.shouldUpdateBackgroundImage = true
  }

  onTextureLoaded(key: string) {
    console.log(`Loaded texture in Phaser with key ${key}`)
    const loadedKeys = this.textures.getTextureKeys()
    this.loadedTextures[key] = true;
    console.log(this.loadedTextures, loadedKeys)
    if (Object.values(this.loadedTextures).filter((value) => !value).length === 0) {
      console.log("Finally loaded everything!")
      this.shouldUpdateAnimations = true
    }
  }

  // TODO this is so nasty... And really inefficient, good thing it's pixelart.
  // Improvements are adding loaders, onLoads, checks on whether everything's really completed and
  // whether it's even necessary that we update.
  updateTextures = () => {
    this.loadedTextures = {};
    this.animations.forEach((animation) => {
      this.animationKeyMap[animation.name] = `ANIM_${Math.random()}`;
      animation.sprites.forEach((sprite, i) => {
        const key = `${animation.name}_${i}`;
        const generatedKey = `A_${Math.random()}`
        this.animationKeyMap[key] = generatedKey;
        this.loadedTextures[generatedKey] = false;
        // if (this.textures.getTextureKeys().includes(key)) {
        //   this.textures.remove(key)
        // }
        const texture = this.textures.addBase64(generatedKey, `data:image/png;base64, ${sprite.contents}`);
        texture.on('onload', (k: string) => this.onTextureLoaded(k))
      })
    })
  }

  updateAnimations = () => {
    this.animations.forEach((animation: IAnimation) => {
      this.anims.create({
        key: this.animationKeyMap[animation.name],
        frames: animation.sprites.map((_, index) => {
          const key = `${animation.name}_${index}`;
          return {key: this.animationKeyMap[key]}
        }),
        frameRate: 1000 / animation.properties!.timeout,
        repeat: -1,
      })
    })
  }

  loadAnimations = (animations: IAnimation[]) => {
    console.log(this);
    this.shouldUpdateTextures = true;
    this.animations = animations;
    console.log(`Loading animations ${animations}`)
    // animations.forEach((animation) => {
    //
    //   animation.sprites.forEach((sprite, i) => {
    //     this.textures?.addBase64(`${animation.name}_${i}`, sprite);
    //   })
    // })
  }

  loadBackgroundImage = (backgroundImage: ISprite) => {
    console.log("Loading background image in Phaser")
    this.shouldLoadBackgroundImage = true;
    this.backgroundImage = backgroundImage;
  }

  private updateBackgroundImage() {
    const key = `background`;
    const generatedKey = `B_${Math.random()}`
    this.animationKeyMap[key] = generatedKey;
    this.loadedTextures[generatedKey] = false;

    const texture = this.textures.addBase64(generatedKey, `data:image/png;base64, ${this.backgroundImage!.contents}`);
    texture.on('onload', (k: string) => this.onBackgroundTextureLoaded(k))
  }
}

function TemplateGame({ width, height }: GameProps) {
  const gameMountRef = useRef<HTMLDivElement>(null)
  const gameRef = useRef<Game>()
  const animations = useSelector(((s: RootState) => s.animations.animations))
  const backgroundSprite = useSelector(((s: RootState) => s.animations.backgroundImage))
  const interactiveSceneRef = useRef<Template>()

  const [loaded, setLoaded] = useState(false);


  useEffect(() => {
    console.log("Updating animations in Phaser useEffect")
    interactiveSceneRef.current?.loadAnimations(animations)
  }, [animations, loaded])

  useEffect(() => {
    console.log("Updating background in Phaser useEffect")
    if (backgroundSprite) {
      console.log("Really updating background in game screen")
      interactiveSceneRef.current?.loadBackgroundImage(backgroundSprite)
    }
  }, [backgroundSprite, loaded])

  const onPreload = () => {
    console.log("Preloaded")

  }

  const onCreate = () => {
    console.log("Created")
  }

  useEffect(() => {
    console.log("Running game creation effect")

    if (!gameRef.current) {
      interactiveSceneRef.current = new Template({
        key: 'template',
      })

      const phaserConfig = {
        type: Phaser.AUTO,
        parent: gameMountRef.current as HTMLDivElement,
        width: width,
        height: height,
        scene: interactiveSceneRef.current,
        antialias: false,
        callbacks: {
          postBoot: () => {
            setLoaded(true)
          }
        }
      }

      gameRef.current = new Phaser.Game(phaserConfig);
      // gameRef.current.scene.scenes[0].loadAnimations(animations)
    }
  }, [])


  return (
    <div ref={gameMountRef}>

    </div>
  );
}

export default TemplateGame;