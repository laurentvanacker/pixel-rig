import React, {useEffect, useState} from 'react';


export interface TimerProps {
  timeout: number;
  onTimeout: () => void;
}

function Timer({ timeout, onTimeout }: TimerProps) {
  const time = new Date()
  const [timer, setTimer] = useState(time)

  useEffect(() => {
    let mounted = true;
    const timeoutSet = setTimeout(() => {
      const now = new Date()
      onTimeout()
      // console.log(now.getTime() / 1000 - time.getTime() / 1000)
      setTimer(now)
    }, timeout)
    return () => {
      clearTimeout(timeoutSet);
    }
  }, [timeout, timer])

  return null;
}

export default Timer;