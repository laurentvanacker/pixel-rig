import React, {ElementRef, ReactNode, useEffect, useRef, useState} from 'react';
import tw from 'twin.macro';
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "store/store";
import {addAnimation, IAnimation, initialState, ISprite, setImage} from "store/slices/animations.slice";
import {Button} from "components/Buttons";

import {ArrowUpShort} from "@styled-icons/bootstrap/ArrowUpShort";
import {ArrowLeftShort} from "@styled-icons/bootstrap/ArrowLeftShort";
import {ArrowRightShort} from "@styled-icons/bootstrap/ArrowRightShort";
import {ArrowDownShort} from "@styled-icons/bootstrap/ArrowDownShort";
import {ZoomIn} from "@styled-icons/bootstrap/ZoomIn"
import {ZoomOut} from "@styled-icons/bootstrap/ZoomOut"
import {FastForward} from "@styled-icons/material/FastForward"
import {FastRewind} from "@styled-icons/material/FastRewind"
import {AnimationSent, ImageSent} from "App";
import Timer from "components/Timer";

const {ipcRenderer} = window.require("electron");

// import tw from 'twin.macro';

export interface AddAnimationProps {
  visible: boolean;
  setVisible: (b: boolean) => void;
}

// const Div = tw.div`border mx-auto bg-purple-300 hover:border-black hover:bg-purple-500 rounded-lg py-2 px-4 text-white max-w-lg`

export interface Template {
  name: string;
  key: string;
  icon: ReactNode;
}

const templates: Template[] = [
  {
    name: "up",
    key: "up",
    icon: <ArrowUpShort tw="w-6 h-6" fill="black"/>,
  },
  {
    name: "down",
    key: "down",
    icon: <ArrowDownShort tw="w-6 h-6" fill="black"/>,
  },
  {
    name: "right",
    key: "right",
    icon: <ArrowRightShort tw="w-8 h-6" fill="black"/>,
  },
  {
    name: "left",
    key: "left",
    icon: <ArrowLeftShort tw="w-6 h-6" fill="black"/>,
  },
]

const CanvasButton = tw.button`text-sm px-1 m-0 rounded border border-black bg-gray-200 hover:bg-gray-300 text-black`


function AddBackgroundImageModal({visible, setVisible}: AddAnimationProps) {
  const [backgroundSprite, setBackgroundSprite] = useState<ISprite | undefined>();
  const [backgroundImage, setBackgroundImage] = useState<HTMLImageElement | undefined>();

  const dispatch = useDispatch()

  const canvasRef = useRef<HTMLCanvasElement | null>(null)

  useEffect(() => {
    ipcRenderer.on('select-background-image-response', (e: any, args: ISprite[]) => {
      const sprite = args[0];
      setBackgroundSprite(sprite)
      const image = new Image();
      image.onload = ((ev: Event) => {
        console.log(`Image loaded ${sprite.filePath}`)
      });
      image.src = `data:image/png;base64, ${sprite.contents}`
      setBackgroundImage(image)
    });
  }, [])

  const draw = (context: CanvasRenderingContext2D) => {
    context.fillStyle = "#000000"
    context.fillRect(0, 0, context.canvas.width, context.canvas.height)
    if (backgroundImage) {
      context.drawImage(backgroundImage, (context.canvas.width - backgroundImage.width) / 2, (context.canvas.height - backgroundImage.height) / 2, backgroundImage.width, backgroundImage.height)
    }
  }

  useEffect(() => {
    if (!visible) {
      return
    }

    const canvas = canvasRef.current as HTMLCanvasElement
    const context = canvas.getContext('2d')
    //

    if (context !== null) {
      context.imageSmoothingEnabled = false
      // draw(context)
      let animationFrameId: number;

      const render = () => {
        draw(context)
        animationFrameId = window.requestAnimationFrame(render)
      }
      render()

      return () => {
        window.cancelAnimationFrame(animationFrameId)
      }
    }
  }, [draw])

  const hide = () => {
    setBackgroundImage(undefined)
    setBackgroundSprite(undefined)
    setVisible(false);
  }

  const save = () => {
    if (backgroundSprite) {
      dispatch(setImage(backgroundSprite))
      ipcRenderer.invoke('set-background-image', backgroundSprite)
      hide()
    }
  }

  const selectFile = () => {
    ipcRenderer.invoke('select-background-image')
  }

  if (!visible) {
    return null;
  }

  return (
    <div tw="fixed z-10 inset-0 overflow-y-auto font-sans">
      <div tw="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div tw="fixed inset-0 transition-opacity" aria-hidden="true">
          <div tw="absolute inset-0 bg-gray-800 opacity-60"></div>
        </div>

        <span tw="sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

        <div
        tw="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all my-8 align-middle max-w-lg w-full"
        role="dialog" aria-modal="true" aria-labelledby="modal-headline">
        <div tw="bg-white px-4 pt-5 pb-4 p-6 pb-4">
          <div tw="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
            <h3 tw="text-lg leading-6 font-medium text-gray-900 text-center" id="modal-headline">
              Set background image
            </h3>
            <div tw="flex flex-row justify-between max-w-lg w-full mt-2">
              <div tw="flex flex-col justify-center">
                <Button tw="h-8 px-2 mr-4 ml-0" onClick={selectFile}>Select Files</Button>
              </div>
              <div tw="flex text-gray-900 content-center">
                <div tw="flex flex-col justify-center pr-6">
                  <p>Preview:</p>
                </div>
                <div>
                  <canvas tw="border border-black" width="256px" height="256px" ref={canvasRef}/>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div tw="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
          <button onClick={save} type="button"
                  tw="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-400 text-base font-medium text-white hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm">
            Add
          </button>
          <button onClick={hide} type="button"
                  tw="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
            Cancel
          </button>
        </div>
      </div>
      </div>
    </div>
  );
}

export default AddBackgroundImageModal;