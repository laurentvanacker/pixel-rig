import React, {useEffect, useRef, useState} from 'react';
import tw from 'twin.macro';
import Timer from "components/Timer";
import {Button} from "components/Buttons";
import {IAnimation, ISprite} from "store/slices/animations.slice";

interface AnimationProps {
  animation: IAnimation;
  onDeleteAnimation: (e: string) => void;
  onEditAnimation: (e: IAnimation) => void;
  backgroundImage?: HTMLImageElement;
}

const CanvasButton = tw.button`text-sm px-1 m-0 rounded border border-black hover:bg-gray-100 bg-gray-300 text-black`

function Animation({animation, onDeleteAnimation, onEditAnimation, backgroundImage} : AnimationProps) {
  const [spriteIndex, setSpriteIndex] = useState(0)
  const [scale, setScale] = useState(animation.properties?.scale || 2.0)
  const {name, button, sprites} = animation
  const imagesRef = useRef<HTMLImageElement[]>([])
  const canvasRef = useRef<HTMLCanvasElement | null>(null)

  const timeout: number = animation.properties?.timeout || 200
  const flip: boolean = animation.properties?.flip || false;

  const draw = (context: CanvasRenderingContext2D) => {
    const image = imagesRef.current[spriteIndex];
    context.fillStyle = "#000000"
    context.fillRect(0, 0, context.canvas.width, context.canvas.height)
    if (backgroundImage) {
      context.drawImage(backgroundImage, (context.canvas.width - backgroundImage.width * scale) / 2, (context.canvas.height - backgroundImage.height * scale) / 2, backgroundImage.width * scale, backgroundImage.height * scale)
    }
    if (flip) {
      context.setTransform(-1, 0, 0, 1, (context.canvas.width), 0)
      context.drawImage(image, (context.canvas.width - image.width * scale) / 2, (context.canvas.height - image.height * scale) / 2, image.width * scale, image.height * scale)
    } else {
      context.setTransform(1, 0, 0, 1, 0, 0)
      context.drawImage(image, (context.canvas.width - image.width * scale) / 2, (context.canvas.height - image.height * scale) / 2, image.width * scale, image.height * scale)
    }
  }

  useEffect(() => {
    imagesRef.current = sprites.map((sprite) => {
      const image = new Image()
      image.onload = (ev) => {
        console.log(`Image loaded ${sprite.filePath}`)
      }
      image.src = `data:image/png;base64, ${sprite.contents}`
      return image;
    });
  }, [sprites])

  useEffect(() => {
    const canvas = canvasRef.current as HTMLCanvasElement
    const context = canvas.getContext('2d')
    //

    if (context !== null) {
      context.imageSmoothingEnabled = false
      draw(context)
      // let animationFrameId: number;
      //
      // const render = () => {
      //   draw(context)
      //   animationFrameId = window.requestAnimationFrame(render)
      // }
      // render()
      //
      // return () => {
      //   window.cancelAnimationFrame(animationFrameId)
      // }
    }
  }, [draw])



  const onTimeout = () => {
    setSpriteIndex((spriteIndex + 1) % sprites.length)
  }

  const onIncreaseScale = () => {
    setScale(scale * 2)
  }

  const onDecreaseScale = () => {
    setScale(scale / 2)
  }

  const resetScale = () => {
    setScale(animation.properties?.scale || 2.0)
  }

  const edit = () => {
    console.log("Editing")
    onEditAnimation(animation)
  }

  const deleteAnimation = () => {
    console.log("Deleting the animation")
    onDeleteAnimation(name)
  }

  return (
    <div tw="m-1">
      <Timer timeout={timeout} onTimeout={onTimeout}/>
      <div tw="flex justify-between bg-gray-600 px-2 text-white"><p>{name.toUpperCase()}</p><p>{button.toUpperCase()}</p></div>
      {/*<img tw="max-w-xl h-40 border-black border hover:bg-gray-400" src={`data:image/png;base64, ${sprites[spriteIndex]}`}  alt={"We can't find the image! Uhoh!"}/>*/}
      <canvas tw="border-black border" width="144px" height="144px" ref={canvasRef} />
      <div tw="flex justify-between">
        <CanvasButton onClick={edit}>E</CanvasButton>
        <div tw="flex items-center justify-center">
          <CanvasButton onClick={onIncreaseScale}>+</CanvasButton>
          <CanvasButton onClick={resetScale}>{(scale * 100).toFixed(1)} %</CanvasButton>
          <CanvasButton onClick={onDecreaseScale}>-</CanvasButton>
        </div>
        <CanvasButton onClick={deleteAnimation}>D</CanvasButton>
      </div>
      </div>
  );
}


export default Animation;