import React from 'react';
import tw from "twin.macro";

export const Button = tw.button`rounded bg-gray-300 text-black border border-black hover:bg-gray-100 ml-5 p-1 focus:border-black`
