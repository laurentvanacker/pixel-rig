import React from 'react';
import tw from 'twin.macro'
import {Button} from "components/Buttons";


export interface AnimationBarProps {
  onTemplatePressed: () => void;
  onCustomPressed: () => void;
}



function AnimationBar({onTemplatePressed, onCustomPressed}: AnimationBarProps) {
  return (
    <div tw="flex justify-between bg-gray-500 text-white px-4 py-2 font-sans">
      <h1 tw="text-lg p-0 m-0">
        Animations
      </h1>
      <div tw="order-last">
        <Button tw="bg-blue-500 text-white hover:bg-blue-600 border-blue-300" onClick={onTemplatePressed}>+ Template</Button>
        {/*<Button onClick={onCustomPressed}>+ Custom</Button>*/}
      </div>
    </div>
  );
}

export default AnimationBar;