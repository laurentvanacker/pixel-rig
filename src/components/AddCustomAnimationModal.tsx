import React, {ElementRef, ReactNode, useEffect, useRef, useState} from 'react';
import tw from 'twin.macro';
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "store/store";
import {addAnimation, IAnimation, initialState, ISprite, setImage} from "store/slices/animations.slice";
import {Button} from "components/Buttons";

import {ArrowUpShort} from "@styled-icons/bootstrap/ArrowUpShort";
import {ArrowLeftShort} from "@styled-icons/bootstrap/ArrowLeftShort";
import {ArrowRightShort} from "@styled-icons/bootstrap/ArrowRightShort";
import {ArrowDownShort} from "@styled-icons/bootstrap/ArrowDownShort";
import {ZoomIn} from "@styled-icons/bootstrap/ZoomIn"
import {ZoomOut} from "@styled-icons/bootstrap/ZoomOut"
import {FastForward} from "@styled-icons/material/FastForward"
import {FastRewind} from "@styled-icons/material/FastRewind"
import {AnimationSent, ImageSent} from "App";
import Timer from "components/Timer";

const {ipcRenderer} = window.require("electron");

// import tw from 'twin.macro';

export interface AddAnimationProps {
  visible: boolean;
  setVisible: (b: boolean) => void;
}

// const Div = tw.div`border mx-auto bg-purple-300 hover:border-black hover:bg-purple-500 rounded-lg py-2 px-4 text-white max-w-lg`

export interface Template {
  name: string;
  key: string;
  icon: ReactNode;
}

const templates: Template[] = [
  {
    name: "up",
    key: "up",
    icon: <ArrowUpShort tw="w-6 h-6" fill="black"/>,
  },
  {
    name: "down",
    key: "down",
    icon: <ArrowDownShort tw="w-6 h-6" fill="black"/>,
  },
  {
    name: "right",
    key: "right",
    icon: <ArrowRightShort tw="w-8 h-6" fill="black"/>,
  },
  {
    name: "left",
    key: "left",
    icon: <ArrowLeftShort tw="w-6 h-6" fill="black"/>,
  },
]

const CanvasButton = tw.button`text-sm px-1 m-0 rounded border border-black bg-gray-200 hover:bg-gray-300 text-black`


function AddCustomAnimationModal({visible, setVisible}: AddAnimationProps) {
  const [spriteIndex, setSpriteIndex] = useState(0)
  const [scale, setScale] = useState(4.0)
  const [flip, setFlip] = useState(false)
  const [timeout, setTimeout] = useState(200)
  const [state, setState] = useState("TEMPLATE")
  const [template, setTemplate] = useState<Template | undefined>()
  const animations = useSelector(((s: RootState) => s.animations.animations))
  const [sprites, setSprites] = useState<ISprite[]>([]);
  const [images, setImages] = useState<HTMLImageElement[]>([]);

  const dispatch = useDispatch()

  const canvasRef = useRef<HTMLCanvasElement | null>(null)

  useEffect(() => {
    ipcRenderer.on('select-sprites-response', (e: any, args: AnimationSent[]) => {
      setSprites(args[0].sprites)
      setImages(args[0].sprites.map((sprite: ISprite) => {
        const image = new Image()
        image.onload = (ev) => {
          console.log(`Image loaded ${sprite.filePath}`)
        }
        image.src = `data:image/png;base64, ${sprite.contents}`
        return image;
      }));
    });
  }, [])

  const draw = (context: CanvasRenderingContext2D) => {
    const image = images[spriteIndex];

    context.fillStyle = "#000000"
    context.fillRect(0, 0, context.canvas.width, context.canvas.height)
    if (images.length > 0) {
      if (flip) {
        context.setTransform(-1, 0, 0, 1, (context.canvas.width), 0)
        context.drawImage(image, (context.canvas.width - image.width * scale) / 2, (context.canvas.height - image.height * scale) / 2, image.width * scale, image.height * scale)
      } else {
        context.setTransform(1, 0, 0, 1, 0, 0)
        context.drawImage(image, (context.canvas.width - image.width * scale) / 2, (context.canvas.height - image.height * scale) / 2, image.width * scale, image.height * scale)
      }
    }
  }

  useEffect(() => {
    if (state === "ANIMATION") {
      const canvas = canvasRef.current as HTMLCanvasElement
      const context = canvas.getContext('2d')
      //

      if (context !== null) {
        context.imageSmoothingEnabled = false
        // draw(context)
        let animationFrameId: number;

        const render = () => {
          draw(context)
          animationFrameId = window.requestAnimationFrame(render)
        }
        render()

        return () => {
          window.cancelAnimationFrame(animationFrameId)
        }
      }
    }
  }, [draw])

  const hide = () => {
    setTemplate(undefined)
    setImages([])
    setSprites([])
    setState("TEMPLATE")
    setVisible(false);
  }

  const save = () => {
    if (sprites.length > 0) {
      const newAnimation = {
        name: template!.name,
        button: template!.key,
        sprites,
        properties: {
          flip,
          timeout,
          scale,
        }
      };
      dispatch(addAnimation(newAnimation))
      ipcRenderer.invoke('add-animation', newAnimation)
      hide()
    }
  }

  const selectTemplate = (template: Template) => {
    setTemplate(template)
    if (state === "TEMPLATE") {
      setState("ANIMATION")
    }
  }

  const onTimeout = () => {
    if (visible && images.length > 0) {
      setSpriteIndex((spriteIndex + 1) % images.length)
    }
  }


  const selectFiles = () => {
    ipcRenderer.invoke("select-sprites")
  }

  const onIncreaseScale = () => {
    setScale(scale * 2)
  }

  const onDecreaseScale = () => {
    setScale(scale / 2)
  }

  const resetScale = () => {
    setScale(1)
  }

  const onSlowDown = () => {
    const updateRate = 1000 / timeout;
    setTimeout(Math.round(1000 / (Math.max(updateRate - 1, 1))))
  }

  const onMakefaster = () => {
    const updateRate = 1000 / timeout;
    setTimeout(Math.round(1000 / (updateRate + 1)))
  }

  const resetSpeed = () => {
    setTimeout(200)
  }

  
  const toggleFlip = () => {
    setFlip(!flip)
  }

  const getContents = () => {
    if (state === "ANIMATION") {
      return (<div
        tw="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all my-8 align-middle max-w-lg w-full"
        role="dialog" aria-modal="true" aria-labelledby="modal-headline">
        <div tw="bg-white px-4 pt-5 pb-4 p-6 pb-4">
          <div tw="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
            <h3 tw="text-lg leading-6 font-medium text-gray-900 text-center" id="modal-headline">
              Add animation {`${template?.name[0].toUpperCase()}${template?.name.slice(1)}`}{template?.icon}
            </h3>
            <div tw="flex flex-row justify-between max-w-lg w-full mt-2">
              <div tw="flex flex-col justify-center">
                <Button tw="h-8 px-2 mr-4 ml-0" onClick={selectFiles}>Select Files</Button>
              </div>
              <div tw="flex text-gray-900 content-center">
                <div tw="flex flex-col justify-center pr-6">
                  <p>Preview:</p>
                </div>
                <div>
                  <div tw="">
                    <div tw="flex items-center justify-center align-top">
                      <CanvasButton tw="mt-0" onClick={toggleFlip}>{flip ? "Flipped" : "Not Flipped"}</CanvasButton>
                    </div>
                    <div tw="flex items-center justify-center align-top">
                      <CanvasButton onClick={onSlowDown}><FastRewind tw="w-4 h-4" fill="black"/></CanvasButton>
                      <CanvasButton tw="mt-0"
                                    onClick={resetSpeed}>{Math.round(1000 / timeout).toFixed(0)} Hz</CanvasButton>
                      <CanvasButton onClick={onMakefaster}><FastForward tw="w-4 h-4" fill="black"/></CanvasButton>
                    </div>
                    <canvas tw="border border-black" width="144px" height="144px" ref={canvasRef}/>
                    <div tw="flex items-center justify-center align-top">
                      <CanvasButton onClick={onDecreaseScale}><ZoomOut tw="w-4 h-4" fill="black"/></CanvasButton>
                      <CanvasButton tw="mt-0" onClick={resetScale}>{(scale * 100).toFixed(1)} %</CanvasButton>
                      <CanvasButton onClick={onIncreaseScale}><ZoomIn tw="w-4 h-4" fill="black"/></CanvasButton>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div tw="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
          <button onClick={save} type="button"
                  tw="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-400 text-base font-medium text-white hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 sm:ml-3 sm:w-auto sm:text-sm">
            Add
          </button>
          <button onClick={hide} type="button"
                  tw="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
            Cancel
          </button>
        </div>
        <Timer timeout={timeout} onTimeout={onTimeout}/>
      </div>)
    } else if (state === "TEMPLATE") {
      const animationNames = animations.map((animation: IAnimation) => animation.name)

      const templateButtons = templates.filter((template) => {
        return !animationNames.includes(template.name);
      }).map((template) => (
        <Button tw="w-32 flex justify-between my-1" key={template.name} onClick={() => selectTemplate(template)}>
          <span tw="capitalize text-left">
            {template.name}
          </span>
          {template.icon}
        </Button>))

      return (<div
        tw="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all align-middle max-w-xs w-full"
        role="dialog" aria-modal="true" aria-labelledby="modal-headline">
        <div tw="bg-white px-4 pt-5 pb-4">
          <div tw="mt-0 text-center">
            <h3 tw="text-lg leading-6 font-medium text-gray-900 text-center" id="modal-headline">
              Templates
            </h3>
            <div tw="mt-2">
              <p tw="text-sm text-gray-500 mb-2">
                Pick a template
              </p>
              <div tw="flex flex-row content-center justify-center">
                <div tw="flex flex-col">
                  {templateButtons}
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>)
    }
  }

  if (!visible) {
    return null;
  }


  const contents = getContents()

  return (
    <div tw="fixed z-10 inset-0 overflow-y-auto font-sans">
      <div tw="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div tw="fixed inset-0 transition-opacity" aria-hidden="true">
          <div tw="absolute inset-0 bg-gray-800 opacity-60"></div>
        </div>

        <span tw="sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

        {contents}
      </div>
    </div>
  );
}

export default AddCustomAnimationModal;