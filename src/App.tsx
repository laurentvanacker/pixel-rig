import React, {useEffect, useState} from 'react';
import './App.css';
import {updateAnimation, setAnimations, ISprite, IAnimation} from 'store/slices/animations.slice'
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "store/store";
import AnimationBar from "components/AnimationBar";
import Animations from "components/Animations";
import 'twin.macro';
import TemplateGame from "components/TemplateGame";
import AddTemplateAnimationModal from "components/AddTemplateAnimationModal";
import {Button} from "components/Buttons";
import AddBackgroundImageModal from "components/AddBackgroundImageModal";
import AddCustomAnimationModal from "components/AddCustomAnimationModal";


const { ipcRenderer } = window.require("electron");

export interface ImageSent {
  image: string;
}

export interface AnimationSent {
  sprites: ISprite[];
}

function App() {
  const animations = useSelector((s: RootState) => s.animations.animations)

  const [addTemplateAnimationVisible, setAddTemplateAnimationVisible] = useState(false)
  const [addCustomAnimationVisible, setAddCustomAnimationVisible] = useState(false)
  const [addBackgroundImageVisible, setAddBackgroundImageVisible] = useState(false)

  useEffect(() => {
    ipcRenderer.on('add-template', onTemplatePressed)
    ipcRenderer.on('add-custom', onCustomPressed)
    ipcRenderer.on('add-background', onAddBackgroundPressed)
  }, [])

  const onAddBackgroundPressed = () => {
    console.log('Opening template modal')
    setAddBackgroundImageVisible(true)
  }

  const onTemplatePressed = () => {
    console.log('Opening template modal')
    setAddTemplateAnimationVisible(true)
  }

  const hideAddAnimation = () => {
    console.log('Hiding template modal')
    setAddTemplateAnimationVisible(false)
  }

  const onCustomPressed = () => {
    setAddCustomAnimationVisible(true)
  }

  return (
    <div tw="bg-gray-600 text-white h-full w-full">
      <AddTemplateAnimationModal visible={addTemplateAnimationVisible} setVisible={setAddTemplateAnimationVisible} />
      <AddCustomAnimationModal visible={addCustomAnimationVisible} setVisible={setAddCustomAnimationVisible} />
      <AddBackgroundImageModal visible={addBackgroundImageVisible} setVisible={setAddBackgroundImageVisible} />
      <div tw="flex flex-row justify-between content-center">
        <div tw="flex justify-center content-center w-44">
          <div tw="flex flex-col content-center py-8">
            <Button tw="bg-blue-500 text-white hover:bg-blue-600 border-blue-300" onClick={onAddBackgroundPressed}>+ Background</Button>

          </div>
        </div>
        <TemplateGame width={256 * 2} height={256 * 1.5} />
        <div tw="w-44">

          <p>

          </p>
        </div>
      </div>
      <AnimationBar onTemplatePressed={onTemplatePressed} onCustomPressed={onCustomPressed} />
      <Animations animations={animations} />
    </div>
  );
}

export default App;
