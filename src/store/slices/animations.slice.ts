import {createSlice, PayloadAction} from '@reduxjs/toolkit';


export interface IAnimationProperties {
  flip: boolean;
  scale: number;
  timeout: number;
}

export interface ISprite {
  filePath: string;
  contents: string;
}

export interface IAnimation {
  name: string;
  button: string;
  sprites: ISprite[];
  properties: IAnimationProperties;
}

export interface AnimationState {
  animations: IAnimation[];
  backgroundImage?: ISprite;
}

export const initialState: AnimationState = {
  animations: [],
  backgroundImage: undefined,
};

const animationSlice = createSlice({
  name: 'tutorial',
  initialState,
  reducers: {
    setImage(state: AnimationState, { payload }: PayloadAction<ISprite>) {
      state.backgroundImage = payload;
    },
    addAnimation(state: AnimationState, { payload }: PayloadAction<IAnimation>) {
      state.animations.push(payload)
    },
    updateAnimation(state: AnimationState, { payload }: PayloadAction<IAnimation>) {
      console.log("Updated animations in store.")
      const existingAnimation = state.animations.find((animation) => animation.name === payload.name)
      existingAnimation!.sprites = payload.sprites;
    },
    deleteAnimation(state: AnimationState, { payload }: PayloadAction<string>) {
      state.animations = state.animations.filter((animation: IAnimation) => animation.name !== payload)
    },
    setAnimations(state: AnimationState, { payload }: PayloadAction<IAnimation[]>) {
      state.animations = payload
    },
    updateAnimationProperties(state: AnimationState, { payload }: PayloadAction<IAnimation>) {
      const existingAnimationIndex = state.animations.findIndex((animation) => animation.name === payload.name)
      state.animations[existingAnimationIndex] = {
        ...state.animations[existingAnimationIndex],
        ...payload,
      }
    },
  },
});

export const { setImage, addAnimation, updateAnimation, updateAnimationProperties, deleteAnimation, setAnimations } = animationSlice.actions;

export const { name, actions, reducer } = animationSlice;
