import { combineReducers } from 'redux';
import { reducer as animations } from '../store/slices/animations.slice';

const rootReducer = combineReducers({
  animations,
});

export default rootReducer;
