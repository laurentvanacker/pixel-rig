import {configureStore, createStore, getDefaultMiddleware, applyMiddleware, Middleware, MiddlewareArray} from '@reduxjs/toolkit';
import { createLogger } from 'redux-logger';
import { forwardToMain, replayActionRenderer, getInitialStateRenderer } from 'electron-redux';
import rootReducer from "store/reducers";



const middleware: Middleware[] = [];

// Logging Middleware
const logger = createLogger({
  level: 'info',
  collapsed: true,
});

// Skip redux logs in console during the tests
if (process.env.NODE_ENV !== 'test') {
  middleware.push(logger);
}

export const storeToGetType = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware()
      .concat(logger),
});


export type RootState = ReturnType<typeof storeToGetType.getState>;



const initialState = getInitialStateRenderer();

export const store = createStore(
  rootReducer,
  initialState as RootState,
  applyMiddleware(
    forwardToMain,
    ...getDefaultMiddleware()
    .concat(logger))
);


replayActionRenderer(store);