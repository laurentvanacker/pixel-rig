import {configureStore, createStore, getDefaultMiddleware, applyMiddleware, Middleware, MiddlewareArray} from '@reduxjs/toolkit';
import { createLogger } from 'redux-logger';
import { forwardToRenderer, triggerAlias, replayActionMain } from 'electron-redux';
import rootReducer from "../../src/store/reducers";


const middleware: Middleware[] = [];

// Logging Middleware
const logger = createLogger({
  level: 'info',
  collapsed: true,
});

// Skip redux logs in console during the tests
if (process.env.NODE_ENV !== 'test') {
  middleware.push(logger);
}

export const store = createStore(
  rootReducer,
  applyMiddleware(
    triggerAlias,
    ...getDefaultMiddleware()
      .concat(logger),
    forwardToRenderer)
);

replayActionMain(store);