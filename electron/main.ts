import {
  app,
  BrowserWindow,
  ipcMain,
  dialog,
  Menu,
  MenuItemConstructorOptions,
  MenuItem
} from 'electron';
import * as path from 'path';
import * as isDev from 'electron-is-dev';
import installExtension, { REACT_DEVELOPER_TOOLS } from "electron-devtools-installer";
import {FSWatcher, readFileSync} from "fs";
import { store } from "./store/main"
import {IAnimation, setAnimations, updateAnimation, setImage} from "../src/store/slices/animations.slice"

const isMac = process.platform === 'darwin';


const fs = require('fs');

let win: BrowserWindow | null = null;

export interface ISprite {
  filePath: string;
  contents: string;
}


export interface IAnimationProperties {
  flip: boolean;
  scale: number;
  timeout: number;
}


class ApplicationData {
  saveFile?: string = undefined

  animations: IAnimation[] = []
  fileWatchers: Record<string, FSWatcher> = {}
  handlers: Record<string, (...args: any[]) => void> = {}
  changedTimers: Record<string, any> = {}

  addAnimation = (newAnimation: IAnimation) => {
    const existingAnimation = this.animations.find((animation) => newAnimation.name === animation.name);
    if (existingAnimation) {
      const filePaths = newAnimation.sprites.map((sprite) => sprite.filePath)
      const existingFilePaths = existingAnimation.sprites.map((sprite) => sprite.filePath)

      const unusedFiles = existingFilePaths.filter((file) => !(new Set(filePaths).has(file)))
      const newFiles = filePaths.filter((file) => !(new Set(existingFilePaths).has(file)))

      newFiles.forEach((file) => {
        console.log(`Adding watcher but animation exists ${file}`)
        this.fileWatchers[file] = fs.watch(file, (eventType: string, filePath: string) => {
          this.animationChanged(filePath)
        })
      })

      unusedFiles.forEach((file) => {
        console.log(`Removing watcher for ${file}`)
        this.fileWatchers[file].close();
        delete this.fileWatchers[file];
      })

      existingAnimation.sprites = newAnimation.sprites;
    } else {
      const filePaths = newAnimation.sprites.map((sprite) => sprite.filePath)

      newAnimation.sprites.forEach((sprite) => {
        const { filePath } = sprite;
        const watcherName = `${newAnimation.name}_${filePath}`
        if (!(watcherName in this.fileWatchers)) {
          console.log(`Adding watcher for new animation ${filePath}`)
          this.fileWatchers[watcherName] = fs.watch(filePath)
          this.fileWatchers[watcherName].on('change', (eventType: string, watchedFile: string) => {
            console.log(`Reloading file contents for watched file ${watchedFile}, full path ${filePath}`)
            sprite.contents = readFileSync(filePath).toString('base64')
            this.animationChanged(newAnimation.name)
          })
        }
      })

      this.animations.push(newAnimation)
    }
  }

  animationChanged = (animationName: string) => {
    console.log(`Animation changed! ${animationName}`)

    if (!(animationName in this.changedTimers)) {
      // changed timers are used to debounce the application.
      this.changedTimers[animationName] = setTimeout(() => {
        const animation = this.animations.find((animation) => animation.name === animationName);
        console.log(`Updating animation ${animation!.name}`)
        if (this.handlers.change) {
          console.log("Calling change handler")
          this.handlers.change(animation)
        } else {
          console.error("We don't have any handler for the changed sprite")
        }
        delete this.changedTimers[animationName]
      }, 200)
    }
  }

  on = (event: string, handler: (...args: any[]) => void) => {
    this.handlers[event] = handler
  }

  removeAnimation = (animationName: string) => {
    const animation: IAnimation = this.animations.find((animation) => animation.name === animationName)!;
    animation.sprites.forEach(({ filePath }: ISprite) => {
      const watcherName = `${animation.name}_${filePath}`;
      if (this.fileWatchers[watcherName]) {
        this.fileWatchers[watcherName].close()
        delete this.fileWatchers[watcherName]
      }
    })
    this.animations = this.animations.filter((animation) => animation.name !== animationName)
  }


  saveData = () => {
    return {
      animations: [...this.animations],
    }
  }

  addBackgroundImage = (backgroundImage: ISprite) => {
    if (this.fileWatchers.background) {
      this.fileWatchers.background.close()
    }

    this.fileWatchers.background = fs.watch(backgroundImage.filePath, (eventType: string, filePath: string) => {
      console.log('Adding file watcher for background image')
      const newData = readFileSync(backgroundImage.filePath).toString('base64');
      store.dispatch(setImage({
        filePath: backgroundImage.filePath,
        contents: newData,
      }))
    })
  }

  loadData = (data: any) => {
    const animations = [...this.animations];
    animations.forEach((animation) => {
      this.removeAnimation(animation.name);
    })

    data.animations.forEach((animation: IAnimation) => {
      this.addAnimation(animation)
    })

    store.dispatch(setAnimations(data.animations))

    if (data.backgroundImage) {
      this.addBackgroundImage(data.backgroundImage)
      store.dispatch(setImage(data.backgroundImage))
    }
  }
}

const applicationData = new ApplicationData()

const onNewFile = (menuItem: MenuItem, browserWindow: BrowserWindow | undefined, event: Electron.KeyboardEvent) => {
  console.log('NEW', menuItem, browserWindow, event)
}

const onLoadFile = async (menuItem: MenuItem, browserWindow: BrowserWindow | undefined, event: Electron.KeyboardEvent) => {
  const chosen = await dialog.showOpenDialog({ properties: ['openFile'], filters: [{extensions: ['pixelrig'], name: 'Pixel Rig file'}] })
  if (chosen.canceled) {
    console.debug("Canceled on load dialog!")
  } else {
    // todo work with promisify of fs here?
    const loadFileText = fs.readFileSync(chosen.filePaths[0])
    const data = JSON.parse(loadFileText)
    applicationData.loadData(data);
  }
}

const onSaveFile = async (menuItem: MenuItem, browserWindow: BrowserWindow | undefined, event: Electron.KeyboardEvent) => {
  console.log(store.getState())
  store.dispatch(setAnimations([]))
  if (applicationData.saveFile) {
    console.log(`Saving to ${applicationData.saveFile}`)
    const saveData = applicationData.saveData()
    fs.writeFileSync(applicationData.saveFile, JSON.stringify(saveData))
  } else {
    await onSaveAsFile(menuItem, browserWindow, event)
  }
}

const onSaveAsFile = async (menuItem: MenuItem, browserWindow: BrowserWindow | undefined, event: Electron.KeyboardEvent) => {
  const chosen = await dialog.showSaveDialog({ filters: [{extensions: ['pixelrig'], name: 'Pixel Rig file'}] })
  if (chosen.canceled) {
    console.debug("Canceled on save dialog!")
  } else {
    // todo work with promisify of fs here?
    const saveData = applicationData.saveData();
    console.log(`Saving save data ${saveData}`)
    fs.writeFileSync(chosen.filePath, JSON.stringify(saveData))
    applicationData.saveFile = chosen.filePath
    // todo use electron-redux!!
  }
}

const onExportGodot = (menuItem: MenuItem, browserWindow: BrowserWindow | undefined, event: Electron.KeyboardEvent) => {
  console.log('EXPORT GODOT', menuItem, browserWindow, event)
}



// todo properly load the animation from the data here at the start of the frontend code
// todo send a signal to the window in the onLoad where the frontend is properly restarted.

const template: MenuItemConstructorOptions[] = [
  // { role: 'fileMenu' }
  {
    label: 'File',
    submenu: [
      { label: 'New', click: onNewFile },
      { label: 'Open', click: onLoadFile },
      { label: 'Save', click: onSaveFile },
      { label: 'Save As...', click: onSaveAsFile },
      { type: 'separator' },
      { label: 'Recent...', submenu: [
          {label: 'No recent files', enabled: false}
        ]},
      { type: 'separator' },
      { label: 'Export...', submenu: [
          {label: 'Godot Animation (.tres)', click: onExportGodot},
        ]},
      { type: 'separator' },
      isMac ? { role: 'close' } : { role: 'quit' }
    ]
  },
  // { role: 'editMenu' }
  {
    label: 'Edit',
    submenu: [
      { role: 'undo' },
      { role: 'redo' },
    ]
  },
  {
    label: 'Animation',
    submenu: [
      { label: 'No options yet...', enabled: false },
    ]
  },
  {
    label: 'Game',
    submenu: [
      { label: 'No options yet...', enabled: false },
    ]
  },
  {
    role: 'help',
    submenu: [
      {role: "about"},
    ]
  },
  // { role: 'viewMenu' }
  {
    label: 'Development',
    submenu: [
      { role: 'reload' },
      { role: 'forceReload' },
      { role: 'toggleDevTools' },
      { type: 'separator' },
      { role: 'resetZoom' },
      { role: 'zoomIn' },
      { role: 'zoomOut' },
      { type: 'separator' },
      { role: 'togglefullscreen' }
    ]
  },
  // { role: 'windowMenu' }

]

const menu = Menu.buildFromTemplate(template)
Menu.setApplicationMenu(menu)

function createWindow() {
  console.log(__dirname)
  win = new BrowserWindow({
    width: 1000,
    height: 734,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true
    },
    icon: __dirname + '/../../public/favicon.ico',
    title: 'Pixel Rig',
  })

  if (isDev) {
    win.loadURL('http://localhost:3000/index.html');
  } else {
    // 'build/index.html'
    win.loadURL(`file://${__dirname}/../index.html`);
  }

  win.on('closed', () => win = null);

  // Hot Reloading
  if (isDev) {
    // 'node_modules/.bin/electronPath'
    require('electron-reload')(__dirname, {
      electron: path.join(__dirname, '..', '..', 'node_modules', '.bin', 'electron'),
      forceHardReset: true,
      hardResetMethod: 'exit'
    });
  }

   ipcMain.handle('select-sprites', async (event, ...args) => {
    const chosen = await dialog.showOpenDialog({ properties: ['openFile', 'multiSelections'] })
    if (!chosen) {
      return;
    }
    const contents = chosen.filePaths.map((file) => ({
      filePath: file,
      contents: readFileSync(file).toString('base64')
    }));
    if (win != null) {
      await win.webContents.send('select-sprites-response', [{
        sprites: contents,
      }])
    }
  })

  ipcMain.handle('select-background-image', async (event, ...args) => {
    const chosen = await dialog.showOpenDialog({ properties: ['openFile'], filters: [{extensions: ["png", "jpg", "jpeg"], name: 'Images'}]})
    if (!chosen) {
      return;
    }

    const file = chosen.filePaths[0];
    const sprite: ISprite = {
      filePath: file,
      contents: readFileSync(file).toString('base64')
    }

    if (win != null) {
      await win.webContents.send('select-background-image-response', [sprite])
    }
  })

  ipcMain.handle('set-background-image', async(event, backgroundSprite: ISprite) => {
    applicationData.addBackgroundImage(backgroundSprite);
  })

  ipcMain.handle('add-animation', async(event, newAnimation: IAnimation) => {
    applicationData.addAnimation(newAnimation);
  })

  ipcMain.handle('remove-animation', async(event, animationName: string) => {
    applicationData.removeAnimation(animationName);
  })


  applicationData.on('change',  (updatedAnimation: IAnimation) => {
    console.log(`We updated the animation ${updatedAnimation.name}`)
    store.dispatch(updateAnimation(updatedAnimation))
  });

  // DevTools
  installExtension(REACT_DEVELOPER_TOOLS)
    .then((name) => console.log(`Added Extension:  ${name}`))
    .catch((err) => console.log('An error occurred: ', err));

  // if (isDev) {
  //   win.webContents.openDevTools();
  // }
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
});

